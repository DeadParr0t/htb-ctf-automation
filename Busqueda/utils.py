def SSH_sudo_command(client,command,password):
    print("\nRunning command on victim machine : {}\n".format(command))
    stdin, stdout, stderr = client.exec_command(command, get_pty=True)
    stdin.write(password+"\n")
    stdin.flush()
    output = stdout.read().decode()

    return output